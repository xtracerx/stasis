# Reference implementation of a statically deployed site with CI.
1. Create a netlify account if you don't already have one.
2. Create a `New Site from Git` and select your fork of this repo.
3. Adjust the `config.toml` to include your own domain and site name.
4. Push your config changes and watch netlify deploy the new commit automatically!